---
title: "Chat Service Frequently Asked Questions"
date: 2020-03-01T16:09:45-04:00
#headline: "The Community for Open Innovation and Collaboration"
#tagline: "The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation."
hide_page_title: true
#hide_sidebar: true
#hide_breadcrumb: true
#show_featured_story: true
#layout: "single"
#links: [[href: "/projects/", text: "Projects"],[href: "/org/workinggroups/", text: "Working Group"],[href: "/membership/", text: "Members"],[href: "/org/value", text: "Business Value"]]
#container: "container-fluid"
---

# Chat Service Frequently Asked Questions

From troubleshooting common issues to learning how to use various chat features, we've got you covered. If you can't find what you're looking for, please don't hesitate to reach out to our support team for further assistance.

You can access our support service by asking on the [#eclipsefdn.chat-support:matrix-staging.eclipse.org](https://chat-staging.eclipse.org/#/room/#eclipsefdn.chat-support:matrix-staging.eclipse.org) room, or by opening an issue in our [HelpDesk](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new).

Check our [support documentation page]({{< relref "/support" >}}) for more details!


## Using accounts from federated servers

**Q: Can I participate in a room with my account from matrix.org or any other matrix server?**

A: By using our chat service, Eclipse members can easily authenticate using their Eclipse Foundation account. We encourage all Eclipse members to use our chat service as their primary point of entry to our Matrix server, but we welcome all Matrix users to participate in our chat rooms and spaces with their existing account from any other matrix server via federation and with any client of their choice.

For those who don't have an Eclipse Foundation account, you can participate in our chat rooms and spaces by simply searching for the room or space you're interested in and joining as you would normally. 

## Phone number setup error 

**Q:Why can't I setup my phone number ?**

A: At this time, the Chat Service instance at the Eclipse Foundation is not configured to support SMS. However, it does support email for notification.

It's also worth noting that some Matrix clients may have built-in support for SMS or other mediums, even if the chat instance at the Eclipse Foundation does not. If you're using a third-party client, check the documentation or settings to see if SMS is supported.


## Room/space creation error

**Q: Why am I receiving a 403 error when trying to create a room or space in Chat service?**

A: The policy of the Matrix server hosted at the Eclipse Foundation does not allow you to create new rooms or spaces.

**Q: What should I do if I need to create a room or space?**

A: If your Eclipse project does not have it's own room or space yet, you can request it by opening a [HelpDesk issue](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new).

**Q: Can I still join existing rooms or spaces even if I cannot create new ones?**

A: Yes, you can still join existing rooms or spaces even if you cannot create new ones. However, you will need to ensure that the rooms or spaces you want to join are not also restricted by permission.


## Encrypted messages are not readable 

**Q: Why am I seeing "Messages not readable" errors?**

A: You may see "Messages not readable" errors if you have received messages that were sent in an encrypted or "not readable" format, but your device or Element Web instance does not have the necessary encryption keys to decrypt and display the messages. To fix this, you can log in to your Matrix account on the device or instance where the messages were originally sent or by contacting the sender to request that they resend the messages in an unencrypted format.

**Q: How can I prevent "Messages not readable" errors in the future?**

A: To prevent "Messages not readable" errors in the future, you should ensure that your device or Element Web instance has the necessary encryption keys to decrypt messages sent in encrypted or "not readable" format. Additionally, you should ensure that your device or instance is up-to-date and running the latest version of Element Web to avoid any compatibility issues.

**NOTE: The chat service hosted at the Eclipse Foundation only allows encrypted messages for 1:1 communication. All rooms are public and not encrypted.**


## Media storage restriction

**Q: What is the maximum media size that can be shared on chat service?**

A: The maximum media size that can be shared or uploaded as media, such as images or videos on the chat service is 100MB per upload with a quotas of 50GB per user.

**Q: Why are there media size restrictions in chat service?**

A: Media size restrictions are in place to prevent the overuse of server resources and to ensure that Matrix remains a fast and reliable communication platform for all users.

**Q: What happens if I try to upload a media file that exceeds the size limit?**

A: If you try to upload a media file that exceeds the size limit, you may receive an error message or the upload may fail. 

**Q: What should I do if I need to share a large media file on chat service but it exceeds the size limit?**

A: If you need to share a large media file on Matrix but it exceeds the size limit, you may want to consider using a third-party service to host the file and sharing the link in Matrix. Alternatively, you can contact the support to see if there are any alternative solutions available.


## Is our server on your federation blacklist?

**Q: How can I determine if my Matrix server is blacklisted?**

A: Currently we have no server on our federation blacklist. This cannot be the reason for your federation problem.

## Matrix client support other than element-web hosted at Eclipse

**Q: Can the support team help with issues related to other Matrix clients?**

A: We regret to inform you that we cannot provide support for any issues that arise from using other Matrix clients.

## Video/voice call

**Q: Can I make voice and video calls using Element Web on the Eclipse Foundation chat service?**

A: No, voice and video calls are currently not supported on the Eclipse Foundation chat service using Element Web.

**Q: Will voice and video calls be supported in the future?**

A: The development team is continuously working to improve and enhance the features of the chat service. While we cannot guarantee that voice and video calls will be supported in the future, we will continue to listen to feedback from our community and prioritize features accordingly.

## Communicate with other protocols such as Slack, Whatsapp, ...

**Q: Why doesn't the Eclipse Foundation currently implement bridges in its chat service?**

A: The Eclipse Foundation has not implemented bridges in its chat service for several reasons:
* Focus on core features: The Foundation is currently focused on improving the core features of its chat service, such as usability, performance, and security. This means that bots may not be a priority at this time.
* Compatibility: bridges require continuous updates and maintenance to ensure that they are compatible with the chat service and other systems. The Eclipse Foundation may want to ensure that any bots it implements are reliable and can be maintained over time.
* Promoting decentralized communication: The Eclipse Foundation is committed to promoting decentralized communication through the Matrix protocol. By relying on bridges to connect with other messaging platforms, it could be seen as undermining this goal.

## Use of bots

**Q: Why doesn't the Eclipse Foundation currently implement bots in its chat service?**

A: The Eclipse Foundation has not implemented bots in its chat service for several reasons:
* Focus on core features: The Foundation is currently focused on improving the core features of its chat service, such as usability, performance, and security. This means that bots may not be a priority at this time.
* Compatibility: Bots require continuous updates and maintenance to ensure that they are compatible with the chat service. The Eclipse Foundation may want to ensure that any bots it implements are reliable and can be maintained over time.
