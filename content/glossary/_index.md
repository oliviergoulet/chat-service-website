---
title: "Chat Service Glossary"
date: 2020-03-01T16:09:45-04:00
#headline: "The Community for Open Innovation and Collaboration"
#tagline: "The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation."
hide_page_title: true
#hide_sidebar: true
#hide_breadcrumb: true
#show_featured_story: true
#layout: "single"
#links: [[href: "/projects/", text: "Projects"],[href: "/org/workinggroups/", text: "Working Group"],[href: "/membership/", text: "Members"],[href: "/org/value", text: "Business Value"]]
#container: "container-fluid"
---

# Chat Service Glossary

* **Matrix**: A decentralized communication protocol. 
* **Synapse**: An implementation of the Matrix protocol, which handles user authentication, rooms/spaces, and message routing. It's a backend server.
* **Element Web**: A web-based client for Matrix server that allows users to access with their Matrix account (mxid) and communicate with others through a web browser.
* **Federation**: The ability of Matrix servers to communicate with each other, allowing users on different servers to interact and communicate.
* **Mxid**: A unique identifier used to represent a Matrix user account. It is in the format of @username:server.domain, where username is the chosen username and server.domain is the domain name of the user's matrix server.
* **Rooms**: The basic unit of communication in Matrix, where users can send messages, share files, and collaborate with others.
* **Spaces**: A higher-level grouping mechanism that allows to organize rooms and other spaces into logical categories or topics.
* **Bridges**: Matrix feature which allow external communication channels, such as third-party chat services or other messaging protocols, to be connected to the Matrix network. The most popular bridging applications are bridges for protocols such as IRC, Slack, Discord, XMPP, etc.
* **Bots**: Aautomated programs that can perform various tasks within the Matrix, such as sending automated messages, providing information, or performing actions on behalf of users.