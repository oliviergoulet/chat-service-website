---
title: "Chat Service"
date: 2023-02-15T16:09:45-04:00
#headline: "The Community for Open Innovation and Collaboration"
#tagline: "The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation."
hide_page_title: true
#hide_sidebar: true
#hide_breadcrumb: true
#show_featured_story: true
#layout: "single"
#links: [[href: "/projects/", text: "Projects"],[href: "/org/workinggroups/", text: "Working Group"],[href: "/membership/", text: "Members"],[href: "/org/value", text: "Business Value"]]
#container: "container-fluid"
---

# Chat Service at Eclipse Foundation [PREVIEW]

Introducing the chat service from the Eclipse Foundation! We provide a modern, feature-rich platform in a federated way for our community to collaborate, connect and build together. Our chat service is built on top of the ![Matrix logo](https://matrix.org/images/matrix-logo.svg "Matrix logo") protocol, an open standard for secure, decentralized communication based on opensource projects [synapse](https://github.com/matrix-org/synapse) and {{< rawhtml >}}
  <img src="https://element.io/images/logo-mark-primary.svg" alt="Element-web logo" style="height: 20px; width:20px;"/>
{{< /rawhtml >}} [element-web](https://github.com/vector-im/element-web/). 

With this service, you can:
* Join and participate in public channels like one of hundreds of Eclipse project rooms and spaces such as  `Adoptium`, `JakartaEE`, [`Oniro`](https://chat-staging.eclipse.org/#/room/#oniro:matrix-staging.eclipse.org), `Eclipse IDE`...
* Contact Eclipse community members and Eclipse Foundation staff directly.

We believe that communication is a cornerstone of successful open-source development, and we're thrilled to offer this new chat service as a resource to our community. Whether you're an individual contributor or part of a large project team, we hope you'll find the Eclipse chat service a valuable tool for collaborating and building great software.

One of the best things about the Eclipse chat service is that you can use your existing Matrix account to participate. That means you don't have to create a separate account just for Eclipse chat – you can use the same account you use for other Matrix services. This makes it easy to stay connected with your existing communities while also participating in the Eclipse ecosystem. 

You can access the chat service using a default web client, which is easy to use and requires no additional software. Alternatively, if you prefer to use a different client, you can connect to our service using any Matrix-compatible client, such as element-web (web/desktop/mobile client), neochat, ...

To get started, simply visit our [chat landing page](https://chat-staging.eclipse.org), sign in or sign up with/for an Eclipse account or check our documentation [Getting Started]({{< relref "/getting-started" >}}). 

>**IMPORTANT ✋**
>
>All contributions you make to our web site are governed by our [Terms Of Use](https://www.eclipse.org/legal/termsofuse.php). Your interactions with the Eclipse Foundation web properties and any information you may provide us about yourself are governed by our [Privacy Policy](https://www.eclipse.org/legal/privacy.php) and must adhere to the [Eclipse Foundation Code of Conduct](https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php) and follow the [Communication Channel Guidelines](https://www.eclipse.org/org/documents/communication-channel-guidelines/).



