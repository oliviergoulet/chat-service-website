---
title: "Chat Service links"
date: 2020-03-01T16:09:45-04:00
#headline: "The Community for Open Innovation and Collaboration"
#tagline: "The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation."
hide_page_title: true
#hide_sidebar: true
#hide_breadcrumb: true
#show_featured_story: true
#layout: "single"
#links: [[href: "/projects/", text: "Projects"],[href: "/org/workinggroups/", text: "Working Group"],[href: "/membership/", text: "Members"],[href: "/org/value", text: "Business Value"]]
#container: "container-fluid"
---

# Chat Service links

* Chat service [Sign in/Sign up page](https://chat-staging.eclipse.org/)
* Eclipse Foundation staff space: [#eclipsefdn:matrix-staging.eclipse.org](https://chat-staging.eclipse.org/#/room/#eclipsefdn:matrix-staging.eclipse.org)
* Eclipse Foundation projects space: [#eclipse:matrix-staging.eclipse.org](https://chat-staging.eclipse.org/#/room/#eclipse:matrix-staging.eclipse.org)
* Chat service support room: [#eclipsefdn.chat-support:matrix-staging.eclipse.org](https://chat-staging.eclipse.org/#/room/#eclipsefdn.chat-support:matrix-staging.eclipse.org)
* Chat service maintenance room: [#eclipsefdn.chat-maintenance:matrix-staging.eclipse.org](https://chat-staging.eclipse.org/#/room/#eclipsefdn.chat-maintenance:matrix-staging.eclipse.org)

# Eclipse links

* Eclipse Foundation community support: [Helpdesk]([chat-staging.eclipse.org/](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues))
* Eclipse Foundation account [Sign in/Sign up](https://accounts.eclipse.org/)

## Legal links

* [Eclipse Foundation Website Privacy Policy](https://www.eclipse.org/legal/privacy.php)
* [Eclipse Foundation Terms of Use](https://www.eclipse.org/legal/termsofuse.php)
* [Copyright Agent](https://www.eclipse.org/legal/copyright.php)
* [Community Code of Conduct](https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php)
* [Eclipse Foundation Legal Resources](https://www.eclipse.org/legal/)